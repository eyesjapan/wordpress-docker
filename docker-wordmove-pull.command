#!/bin/bash
cd $(dirname $BASH_SOURCE) || {
    echo Error getting script directory >&2
    exit 1
}

docker exec wp_wordmove wordmove pull -e production -d 